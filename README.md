# Airbnb Listings & Reviews

**https://www.kaggle.com/code/leilasaffarian/airbnb-eda-final**

 Airbnb data for 250,000+ listings in 10 major cities, including information about hosts, pricing, location, and room type, along with over 5 million historical reviews.  

Analyse this dataset for answer this questions:  

A. Spot any major differences in the Airbnb market between cities.  
    
B. Determind which attributes have the biggest influence in price.  
    
C. Identify any trends or seasonality in the review data.  
    
D. Determind which city offers a better value for travel.  


### Question A: spot any major differences in the Airbnb market between cities:  
1.In order, the cities with the highest number of hosts on Airbnb are Paris, New York, Sydney, Rome, Rio de Janeiro, Istanbul, Mexico City, Bangkok, Cape Town, and Hong Kong.  
2. In order, the most expensive cities on Airbnb are Cape Town, Bangkok, Mexico City, Hong Kong, Rio de Janeiro, Istanbul, Sydney, New York, Paris, and Rome.  
3. In all countries (except Hong Kong), 'Entire place' and 'Private' rooms are the most common types of accommodations on Airbnb.  
4. In all cities, the number of superhosts is lower than the number of non-superhosts.  
  
  
### Question B: Determind which attributes have the biggest influence in price:
1. The majority of hosts have prices ranging between 0 and 4000.   
2. Superhosts tend to be more expensive.  
3. Hosts who respond to requests within a day are typically more expensive.  
4. Hosts without a profile picture are generally more expensive. However, most hosts on Airbnb have pictures.  
5. Hosts without a verified ID tend to be more expensive.  
6. Entire villas, rooms in heritage hotels, and boats are the most expensive types of hosts on Airbnb.  
7. Hotel rooms, entire places, shared rooms, and private rooms are listed from most expensive to least expensive.  
8. Bookable hosts are generally more expensive than non-bookable hosts.  
  
  
### Question C: Identify any trends or seasonality in the review data:
There is seasonality in the review data, with more reviews occurring from August to October than in other months.  


### Question D: determind which city offers a better value for travel:
Depending on the budget, the destination may vary; however, non-superhost shared rooms in Rome are the most affordable option for travel.  

  

